
Drupal Guestbook Smileys (DGBS) provides Smileys module integration for
the Drupal Guestbook (DGB) module.

Requirements
--------------------------------------------------------------------------------
- This module is written for Drupal 6.0+.

- The Drupal Guestbook (DGB) module.
- The Smileys module.

Installation
--------------------------------------------------------------------------------
Copy the Drupal Guestbook Smileys module folder to your module directory and then
enable on the admin modules page.

Administration
--------------------------------------------------------------------------------
Administer the Smileys module settings in: admin/settings/smileys/configure

